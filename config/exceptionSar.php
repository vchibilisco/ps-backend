<?php
//Errores de backend
return [
	//Login
	'invalid_credentials' => 'DNI y/o Password invalidos.',
	'could_not_create_token' => 'Error al crear la validación. Comunicarse con los Administradores de SAR.',
	'user_not_enabled' => 'Debe habilitar el Usuario para realizar esta acción.',
	'could_not_validate_hash' => 'No se pudo validar el hash.',
	'hash_not_exist' => 'No existe el hash. Comunicarse con el Administrador de SAR.',
	'error_update_password' => 'Error al tratar de actualizar el password.',
	'invalid_profiles' => 'No posee un perfil para iniciar sesion mediante este login.',
	'user_duplicate' => 'El usuario ya existe.',
	'user_duplicate_dni_unique' => 'Ya existe un Usuario con este DNI. Por favor verifique la información.',
	'user_duplicate_email_unique' => 'Ya existe un Usuario con este EMAIL. Por favor verifique la información.',
	'user_duplicate_expedient' => 'Ya existe un Usuario con este EXPEDIENTE. Por favor verifique la información.'
];