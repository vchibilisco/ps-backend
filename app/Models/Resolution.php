<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Resolution extends Model
{
	use SoftDeletes;

  protected $table = 'resolutions';

	protected $fillable = array('id','tresolution','nresolution','filepath','description','dateresolution');

	protected $hidden = ['updated_at','created_at'];

	protected $dates = ['deleted_at'];

	public function tresolution()
  {
    return $this->hasOne('App\Models\TypeResolution','id','tresolution');
  }
}
