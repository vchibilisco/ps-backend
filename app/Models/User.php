<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
  use SoftDeletes;

  protected $table = 'user';

	protected $fillable = array('id','dni','name','lastname','email','address','phone','photo','profile','status','expedient');

	protected $hidden = ['updated_at','created_at'];

  protected $dates = ['deleted_at'];

	public function careers()
  {
    return $this->belongsToMany('App\Models\Career','user_career','user','career');
  }

	public function profiles()
  {
    return $this->belongsToMany('App\Models\Profile','user_profile','user','profile');
  }  
}


    