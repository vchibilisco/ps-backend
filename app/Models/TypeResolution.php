<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeResolution extends Model
{
	use SoftDeletes;

  protected $table = 'type_resolutions';

	protected $fillable = array('id','area','description');

	protected $hidden = ['updated_at','created_at'];

	protected $dates = ['deleted_at'];

	public function area()
  {
    return $this->hasOne('App\Models\AreaResolution','id','area');
  }
}
