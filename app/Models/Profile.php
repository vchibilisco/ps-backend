<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
  protected $table = 'profile';

	protected $fillable = array('id','cod','description');

	protected $hidden = ['updated_at','created_at'];
}
