<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentsRes extends Model
{
	use SoftDeletes;

  protected $table = 'student_res';

	protected $fillable = array('id','student','resolution');

	protected $hidden = ['updated_at','created_at'];

	protected $dates = ['deleted_at'];

	public function resolution()
	{
		return $this->hasOne('App\Models\Resolution','id','resolution');
	}
}
