<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Career extends Model
{
	use SoftDeletes;

  protected $table = 'career';

	protected $fillable = array('id','description');

	protected $hidden = ['updated_at','created_at'];

	protected $dates = ['deleted_at'];

	public function users()
  {
    return $this->belongsToMany('App\Models\User','user_career','career','user');
  }
}
