<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
  protected $table = 'user_profile';

	protected $fillable = array('id','user','profile');

	protected $hidden = ['updated_at','created_at'];
}
