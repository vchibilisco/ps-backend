<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AreaResolution extends Model
{
  protected $table = 'area_resolution';

	protected $fillable = array('id','description');

	protected $hidden = ['updated_at','created_at'];
}
