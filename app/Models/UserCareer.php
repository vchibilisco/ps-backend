<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCareer extends Model
{
  protected $table = 'user_career';

	protected $fillable = array('id','user','dni','career','status');

	protected $hidden = ['updated_at','created_at'];
}
