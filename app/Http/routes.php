<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', function () {
    return view('index');
});

Route::post('api/authentication', 'LoginController@Login');
Route::get('api/download/{filename}', 'ResolutionsController@download');
Route::post('api/validatehash', 'LoginController@ValidateHash');
Route::post('api/updatepassword', 'LoginController@UpdatePassword');

Route::post('api/notrescareer', 'EmailController@NotificationResolutionCareer');
Route::post('api/forgotpass', 'EmailController@ForgotPassword');

Route::group(['prefix' => 'api','middleware' => ['jwt.auth']], function () {
    
    Route::post('sendemail', 'EmailController@EnabledUser');
    Route::post('notificationresolution', 'EmailController@NotificationResolution');
    Route::resource(
        '/profile', 
        'ProfileController',
        ['only' => ['index']]
    );

    Route::resource(
        '/career', 
        'CareerController',
        ['only' => ['index','store','show','update','destroy']]
    );

    Route::resource(
        '/typeresolutions', 
        'TypeResolutionController',
        ['only' => ['index','store','show','update','destroy']]
    );

    Route::post('softdeletesec', 'SecUserController@SoftDelete');
    Route::resource(
        '/secUser', 
        'SecUserController',
        ['only' => ['index','store','show','update','destroy']]
    );

    Route::post('softdeletecar', 'CarUserController@SoftDelete');
    Route::resource(
        '/careerUser', 
        'CarUserController',
        ['only' => ['index','store','show','update','destroy']]
    );

    Route::post('softdeletestd', 'StudUserController@SoftDelete');
    Route::resource(
        '/studUser', 
        'StudUserController',
        ['only' => ['index','store','show','update','destroy']]
    );
    
    Route::post('dropcareerres', 'ResolutionsController@dropcareerres');
    Route::post('dropuserres', 'ResolutionsController@dropuserres');
    Route::resource(
        '/resolution', 
        'ResolutionsController',
        ['only' => ['index','store','destroy']]
    );

    Route::resource(
        '/arearesolution', 
        'AreaResolutionController',
        ['only' => ['index']]
    );

    Route::resource(
        '/reports', 
        'ReportsController',
        ['only' => ['index']]
    );
});