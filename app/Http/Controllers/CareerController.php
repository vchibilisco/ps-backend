<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Career;
use App\Models\User;
use Response;
use Input;
use JWTAuth;
use DB;

class CareerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payload = JWTAuth::parseToken()->getPayload();
        if(in_array('GR_SROOT', $payload['user']['profile']) || in_array('GR_SECRE',$payload['user']['profile'])){
            return Response::json([
                'careers' => Career::orderBy('id','asc')->get()
            ]);
        }else{
            $user = User::find($payload['user']['id']);
            return Response::json([
                'careers' => $user->careers()->get()
            ]);    
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $input = Input::all();
        Career::create($input);
        return Response::json($input);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $career = Career::find($id);
        return Response::json([
            'career' => $career
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = Input::all();
        $result = Career::find($id)->update($input);
        return Response::json($result);
    }

        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $career = Career::find($id);
            $request = $career->delete();
            DB::commit();
            return Response::json($request);
        } catch (Exception $e) {
            DB::rollback();
            return Response::json($e);
        }
    }
}
