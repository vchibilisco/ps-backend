<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Response;
use Input;
use DB;
use App\Models\Resolution;
use App\Models\TypeResolution;
use App\Models\CareerRes;
use App\Models\StudentsRes;
use App\Models\Profile;
use JWTAuth;
use DateTime;
use Exception;

class ResolutionsController extends Controller
{
    private function getFileName($request, $file)
    {
        $datetime = new DateTime();
        $payload = JWTAuth::parseToken()->getPayload();
        $nres = str_replace("/","_",$request['nresolution']);
        return $payload['user']['id'].'_'.$payload['user']['dni'].'_'.$nres.'_'.$request['tresolution'].'_'.$datetime->getTimestamp().'_'.$file->getClientOriginalName();
    }

    private function getResolutionsBySec()
    {
        $resultArray = array();
        $resolutions = Resolution::orderBy('dateresolution', 'desc')->get();
        foreach ($resolutions as $res) {
            $tresolution = $res->tresolution()->first();
            if($tresolution['area'] == 3){
                if(isset($tresolution)){
                    $res['tresolution'] = $tresolution;
                }else{
                    $res['tresolution'] = $res->tresolution()->onlyTrashed()->first();
                }
                array_push($resultArray, $res);
            }
        }
        return $resultArray;
    }

    private function getResolutionsByCar($idcareer)
    {
        $careers = CareerRes::where('career','=',$idcareer)->get();
        foreach ($careers as $career) {
             $resolution = Resolution::find($career['resolution']);
             if(isset($resolution)){
                $tresolution = $resolution->tresolution()->first();
                if(isset($tresolution)){
                    $resolution['tresolution'] = $tresolution;
                }else{
                    $resolution['tresolution'] = $resolution->tresolution()->onlyTrashed()->first();
                }
             }
             $career['resolution'] = $resolution;
        }
        return $careers;
    }

    private function getResolutionsByStud($idstud)
    {
        $students = StudentsRes::where('student','=',$idstud)->get();
        foreach ($students as $st) {
            $resolution = Resolution::find($st['resolution']);
            if(isset($resolution)){
                $tresolution = $resolution->tresolution()->first();
                if(isset($tresolution)){
                    $resolution['tresolution'] = $tresolution;
                }else{
                    $resolution['tresolution'] = $resolution->tresolution()->onlyTrashed()->first();
                }
            }

            $st['resolution'] = $resolution;
        }
        return $students;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $payload = JWTAuth::parseToken()->getPayload();
            switch ($request['section']) {
                case 'secre-res':
                    $response = $this->getResolutionsBySec();
                    break;
                case 'career-res':
                    $response = $this->getResolutionsByCar($request['idcareer']);
                    break;
                case 'student-res':
                    $response = $this->getResolutionsByStud($request['idstudent']);
                    break;
                default:
                    $response = null;
                    break;
            }
            return Response::json([
                'resolutions' => ($response != null) ? $response : array()
            ]);
        } catch (Exception $e) {
            return Response::json($e);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Input::all();
        $file = Input::file('fileres');
        $size = $file->getSize();
        try {
            DB::beginTransaction();
            $model = $input['request'];
            $model['filepath'] = $this->getFileName($input['request'], $file);
            switch ($model['parent']['section']) {
                case 'secre-res':
                    if($file->isValid()){
                        $result = Resolution::create($model);
                        Storage::disk('local')->append($model['filepath'], File::get($file));
                    }else{
                        new Exception('File invalid');
                    }                    
                    break;
                case 'career-res':
                    if($file->isValid()){
                        Resolution::create($model);
                        $resolution = Resolution::where('tresolution','=',$model['tresolution'])
                                            ->where('nresolution','=',$model['nresolution'])
                                            ->where('description','=',$model['description'])
                                            ->where('filepath','=',$model['filepath'])->get();
                        if(sizeof($resolution) == 1){
                            $career['career'] = $model['parent']['idcareer'];
                            $career['resolution'] = $resolution[0]['id'];
                            CareerRes::create($career);
                            Storage::disk('local')->append($model['filepath'], File::get($file));
                        }else{
                            new Exception('Error save CareerRes');
                        }
                    }
                    break;
                case 'student-res':
                    if($file->isValid()){
                        Resolution::create($model);
                        $resolution = Resolution::where('tresolution','=',$model['tresolution'])
                                            ->where('description','=',$model['description'])
                                            ->where('filepath','=',$model['filepath'])->get();
                        if(sizeof($resolution) == 1){
                            $student['student'] = $model['parent']['idstudent'];
                            $student['resolution'] = $resolution[0]['id'];
                            StudentsRes::create($student);
                            Storage::disk('local')->append($model['filepath'], File::get($file));
                        }else{
                            new Exception('Error save CareerRes');
                        }
                    }
                    break;
                default:
                    $response = null;
                    break;
            }

            DB::commit();
            return Response::json(true);    
        } catch (Exception $e) {
            DB::rollback();
            return Response::json($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $res = Resolution::find($id);
            if (Storage::exists($res['filepath']))
            {
                Storage::delete($res['filepath']);
            }
            Resolution::destroy($id);
            DB::commit();
            return Response::json(true);
        } catch (Exception $e) {
            DB::rollback();
            return Response::json($e);
        }
    }

    public function dropcareerres(Request $request)
    {
        try {
            DB::beginTransaction();
            $carRes = CareerRes::find($request['id']);
            $res = $carRes->resolution()->first();
            if (Storage::exists($res['filepath']))
            {
                Storage::delete($res['filepath']);
            }
            $carRes->delete();
            $carRes->resolution()->delete();

            DB::commit();
            return Response::json(true);
        } catch (Exception $e) {
            DB::rollback();
            return Response::json($e);
        }
    }

    public function dropuserres(Request $request)
    {
        try {
            DB::beginTransaction();
            $stRes = StudentsRes::find($request['id']);
            $res = $stRes->resolution()->first();
            if (Storage::exists($res['filepath']))
            {
                Storage::delete($res['filepath']);
            }
            $stRes->delete();
            $stRes->resolution()->delete();

            DB::commit();
            return Response::json(true);
        } catch (Exception $e) {
            DB::rollback();
            return Response::json($e);
        }
    }

    public function download($request)
    {
        $pathToFile=storage_path()."/app/".$request;
        return response()->download($pathToFile); 
    }
}
