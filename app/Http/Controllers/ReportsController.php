<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Exception;
use DB;
use App;
use PDF;

class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            if($request['tresolution'] > -1){
                $students = ($request['career'] == 'all')
                ? $this->GetStudentsWithAllCareersTResolutionStatus($request) 
                : $this->GetStudentsWithCareerTResolutionStatus($request);
            }else{
                $students = ($request['career'] == 'all')
                ? $this->GetStudentsWithAllCareerStatus($request)
                : $this->GetStudentsWithCareerStatus($request);
            }
            $pos = 1;
            for ($i=0; $i < sizeof($students) ; $i++) { 
                $students[$i]->position = $pos;
                $pos ++;
            }

            return Response::json(['students' => $students]);
        }catch(Exception $e){
            return Response::json($e);
        }
    }

    private function GetStudentsWithCareerTResolutionStatus($request)
    {
        return DB::table('user')
            ->join('user_profile', function ($join) {
                $join->on('user_profile.user', '=', 'user.id')
                     ->where('user_profile.profile', '=', 2);
                })
            ->join('user_career', function ($join) use ($request) {
                $join->on('user_career.user', '=', 'user.id')
                     ->where('user_career.career', '=', $request['career']);
                })
            ->join('student_res', 'student_res.student', '=', 'user.id')
            ->join('resolutions', function ($join) use ($request){
                $join->on('resolutions.id', '=', 'student_res.resolution')
                     ->where('resolutions.tresolution', '=', $request['tresolution']);
                })
            ->where('user.status','=',$request['status'])
            ->whereNull('user.deleted_at')
            ->get();
    }

    private function GetStudentsWithCareerStatus($request)
    {
        return DB::table('user')
            ->join('user_profile', function ($join) {
                $join->on('user_profile.user', '=', 'user.id')
                     ->where('user_profile.profile', '=', 2);
                })
            ->join('user_career', function ($join) use ($request) {
                $join->on('user_career.user', '=', 'user.id')
                     ->where('user_career.career', '=', $request['career']);
                })
            ->where('user.status','=',$request['status'])
            ->whereNull('user.deleted_at')
            ->get();
    }

    private function GetStudentsWithAllCareersTResolutionStatus($request)
    {
        return DB::table('user')
            ->join('user_profile', function ($join) {
                $join->on('user_profile.user', '=', 'user.id')
                     ->where('user_profile.profile', '=', 2);
                })
            ->join('user_career', 'user_career.user', '=', 'user.id')
            ->join('career', 'career.id', '=', 'user_career.career')
            ->join('student_res', 'student_res.student', '=', 'user.id')
            ->join('resolutions', function ($join) use ($request){
                $join->on('resolutions.id', '=', 'student_res.resolution')
                     ->where('resolutions.tresolution', '=', $request['tresolution']);
                })
            ->where('user.status','=',$request['status'])
            ->whereNull('user.deleted_at')
            ->get();
    }

    private function GetStudentsWithAllCareerStatus($request)
    {
        return DB::table('user')
            ->join('user_profile', function ($join) {
                $join->on('user_profile.user', '=', 'user.id')
                     ->where('user_profile.profile', '=', 2);
                })
            ->join('user_career', 'user_career.user', '=', 'user.id')
            ->join('career', 'career.id', '=', 'user_career.career')
            ->where('user.status','=',$request['status'])
            ->whereNull('user.deleted_at')
            ->get();
    }
}
