<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Mail;
use Config;
use App\Models\User;
use App\Models\Authentication;
use App\Models\Career;
use DB;
use Hash;

class EmailController extends Controller
{
    public function EnabledUser(Request $request)
    {
        try{
            DB::beginTransaction();
            $user = User::find($request['userid']);    
            $user['status'] = 0;
            $user->save();
            $auth = Authentication::where('dni','=',$user['dni'])->first();
            $pass = $user['dni'].time();
            if($auth == null){
                $auth['dni'] = $user['dni']; 
                $auth['password'] = Hash::make($pass);
                $auth['user'] = $user['id'];
                $auth['status'] = 0;
                $auth['hash_enabled'] = md5($pass);
                Authentication::create($auth);
            }else{
                $auth['password'] = Hash::make($pass); 
                $auth['status'] = 0;
                $auth['hash_enabled'] = md5($pass);
                $auth->save();
            }
            $data = array(
                'url' => Config::get('app.urlposgrado').'#/home?resetpass='.$auth['hash_enabled'],
                'pass' => $pass
            );
            $emailTo = $user['email'];
            Mail::send('emails.welcome', $data, function ($message) use ($emailTo){
                $message->from('sar@csnat.unt.edu.ar', 'Posgrado');
                $message->to($emailTo)->subject('Cuenta habilitada');
            });
            DB::commit();
            return Response::json(true);
        }catch(Exception $e){
            DB::rollback();
            return Response::json($e);
        }
    }

    public function NotificationResolution(Request $request)
    {
        try{
            $user = User::find($request['userid']);
            if ($user['status']==1){
                $data = array(
                    'url' => Config::get('app.urlposgrado')
                );
                $emailTo = $user['email'];
                Mail::send('emails.notificationresolution', $data, function ($message) use ($emailTo){
                    $message->from('sar@csnat.unt.edu.ar', 'Posgrado');
                    $message->to($emailTo)->subject('Notificacion');
                });
                return Response::json(true);
            }else {
                return response()->json(['error' => Config::get('exceptionSar.user_not_enabled')], 500); 
            }
        }catch(Exception $e){
            return Response::json($e);
        }
    }

    public function NotificationResolutionCareer(Request $request)
    {
        try{
            $careers = Career::where('id','=',$request['career'])->get();
            $emails = array();
            foreach ($careers as $cr) {
                $users = $cr->users()->get();
                foreach ($users as $us) {
                    if($us->profiles()->where('cod','=','GR_DIRECT')->first() != null){
                        array_push($emails, $us->email);
                    }
                }
            }
            $data = array(
                'url' => Config::get('app.urlposgrado')
            );
            Mail::send('emails.notifrescareer', $data, function ($message) use ($emails){
                $message->from('sar@csnat.unt.edu.ar', 'Posgrado');
                //$message->to($emailTo)->subject('Notificacion');
                foreach ($emails as $email) {
                    $message->to($email);
                }

                $message->subject('Notificacion');
            });
            return Response::json(true);
            
        }catch(Exception $e){
            return Response::json($e);
        }
    }

    public function ForgotPassword(Request $request)
    {
        try{
            DB::beginTransaction();
            $user = User::where("email","=",$request['email'])->first();
            $user['status'] = 0;
            $user->save();
            $auth = Authentication::where('dni','=',$user['dni'])->first();
            $pass = $user['dni'].time();
            if($auth == null){
                $auth['dni'] = $user['dni']; 
                $auth['password'] = Hash::make($pass);
                $auth['user'] = $user['id'];
                $auth['status'] = 0;
                $auth['hash_enabled'] = md5($pass);
                Authentication::create($auth);
            }else{
                $auth['password'] = Hash::make($pass); 
                $auth['status'] = 0;
                $auth['hash_enabled'] = md5($pass);
                $auth->save();
            }
            $data = array(
                'url' => Config::get('app.urlposgrado').'#/home?resetpass='.$auth['hash_enabled'],
                'pass' => $pass
            );
            $emailTo = $user['email'];
            Mail::send('emails.forgotpass', $data, function ($message) use ($emailTo){
                $message->from('sar@csnat.unt.edu.ar', 'Posgrado');
                $message->to($emailTo)->subject('Recuperar Contraseña');
            });
            DB::commit();
            return Response::json(true);
        }catch(Exception $e){
            DB::rollback();
            return Response::json($e);
        }
    }
}
