<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserCareer;
use App\Models\Profile;
use App\Models\Career;
use Response;
use Input;
use DB;
use Exception;
use JWTAuth;
use Config;

class StudUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payload = JWTAuth::parseToken()->getPayload();
        if(in_array('GR_SROOT', $payload['user']['profile']) || in_array('GR_SECRE',$payload['user']['profile'])){
            $profile = Profile::where('cod','=','GR_STUDS')->first();
            $users = User::orderBy('expedient')->get();
            $result = array();
            foreach ($users as $us) {
                $userprofile = $us->profiles()->where('profile','=',$profile->id)->get();
                if( sizeof($userprofile) > 0){
                    array_push($result, $this->getCareer($us));
                }
            }
            return Response::json([
                'users' => $result
            ]);
        }else if(in_array('GR_DIRECT', $payload['user']['profile'])){
            $result = array();
            $profile = Profile::where('cod','=','GR_STUDS')->first();
            foreach ($payload['user']['careers'] as $career) {
                $usersByCareer = Career::find($career['id'])->users()->get();
                foreach ($usersByCareer as $us) {
                    $userprofile = $us->profiles()->where('profile','=',$profile->id)->get();
                    if( sizeof($userprofile) > 0){
                        array_push($result, $this->getCareer($us));
                    }
                }
            }
            return Response::json([
                'users' => $result
            ]);
        }else{
            $user = User::find($payload['user']['id']);
            $user = $this->getCareer($user);
            return Response::json([
                'users' => [$user]
            ]);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            DB::beginTransaction();
            $input = Input::all();
            $insert['expedient'] = $input['expedient'];
            $insert['dni'] = $input['dni'];
            $insert['name'] = $input['name'];
            $insert['lastname'] = $input['lastname'];
            $insert['email'] = $input['email'];
            if(isset($input['address'])){
                $insert['address'] = $input['address'];
            }
            if(isset($input['phone'])){
                $insert['phone'] = $input['phone'];
            }
            if(User::where('expedient','=',$input['expedient'])->first() != null){
                return response()->json(['error' => Config::get('exceptionSar.user_duplicate_expedient')], 500);
            }
            if(User::where('dni','=',$input['dni'])->first() != null){
                return response()->json(['error' => Config::get('exceptionSar.user_duplicate_dni_unique')], 500);
            }
            if(User::where('email','=',$input['email'])->first() != null){
                return response()->json(['error' => Config::get('exceptionSar.user_duplicate_email_unique')], 500);
            }
            $result = User::create($insert);
            $user = User::where('dni','=',$insert['dni'])->first();
            for ($i=0; $i < sizeof($input['careers']); $i++) { 
                $user->careers()->attach($input['careers'][$i]['id']);
            }
            for($i=0; $i < sizeof($input['profile']); $i++) {
                $pr = Profile::where('cod','=',$input['profile'][$i]['cod'])->first();
                $user->profiles()->attach($pr->id);
            }
            DB::commit();
            return Response::json(true);
        }catch(Exception $ex){
            DB::rollback();
            if(strpos($ex->getMessage(), 'Duplicate entry')){
                if (strpos($ex->getMessage(), 'dni_UNIQUE')) {
                    return response()->json(['error' => Config::get('exceptionSar.user_duplicate_dni_unique')], 500);
                }else if (strpos($ex->getMessage(), 'email_UNIQUE')) {
                    return response()->json(['error' => Config::get('exceptionSar.user_duplicate_email_unique')], 500);
                }
            }else{
                return response()->json(['error' => $ex->getMessage()], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $user['careers'] = $user->careers;
        $user['profile'] = $user->profiles;
        return Response::json([
            'user' => $user
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            DB::beginTransaction();
            $input = Input::all();
            $update['expedient'] = $input['expedient'];
            $update['dni'] = $input['dni'];
            $update['name'] = $input['name'];
            $update['lastname'] = $input['lastname'];
            $update['email'] = $input['email'];
            $update['address'] = $input['address'];
            $update['phone'] = $input['phone'];
            if(User::where('expedient','=',$input['expedient'])->where('id','<>',$id)->first() != null){
                return response()->json(['error' => Config::get('exceptionSar.user_duplicate_expedient')], 500);
            }
            if(User::where('dni','=',$input['dni'])->where('id','<>',$id)->first() != null){
                return response()->json(['error' => Config::get('exceptionSar.user_duplicate_dni_unique')], 500);
            }
            if(User::where('email','=',$input['email'])->where('id','<>',$id)->first() != null){
                return response()->json(['error' => Config::get('exceptionSar.user_duplicate_email_unique')], 500);
            }
            $user = User::find($id);
            $result = $user->update($update);
            $user->careers()->detach();
            for ($i=0; $i < sizeof($input['careers']); $i++) {
                $user->careers()->attach($input['careers'][$i]['id']);
            }
            $user->profiles()->detach();
            for ($i=0; $i < sizeof($input['profile']); $i++) {
                $pr = Profile::where('cod','=',$input['profile'][$i]['cod'])->first();
                $user->profiles()->attach($pr->id);
            }
            DB::commit();
            return Response::json($result);
        }catch(Exception $ex){
            DB::rollback();
            if(strpos($ex->getMessage(), 'Duplicate entry')){
                if (strpos($ex->getMessage(), 'dni_UNIQUE')) {
                    return response()->json(['error' => Config::get('exceptionSar.user_duplicate_dni_unique')], 500);
                }else if (strpos($ex->getMessage(), 'email_UNIQUE')) {
                    return response()->json(['error' => Config::get('exceptionSar.user_duplicate_email_unique')], 500);
                }
            }else{
                return response()->json(['error' => $ex->getMessage()], 500);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       try{
            DB::beginTransaction();
            $user = User::find($id);
            $user->status = !$user->status;
            $user->save();
            DB::commit();
            return Response::json(true);
        }catch(Exception $ex){
            DB::rollback();
            return Response::json($e); 
        }
    }

    private function getCareer($user)
    {
        $careers = $user->careers()->get();
        $user['crdescription'] = '';
        foreach ($careers as $cr) {
            if($user['crdescription'] == ''){
                $user['crdescription'] = $cr->description;
            }else{
                $user['crdescription'] = $user['crdescription'].', '.$cr->description;
            }
        }
        return $user;
    }

    public function SoftDelete(Request $request)
    {
        try{
            DB::beginTransaction();
            $user = User::find($request['userid']);
            $user->status = !$user->status;
            $user->save();
            $user->delete();
            DB::commit();
            return Response::json(true);
        }catch(Exception $ex){
            DB::rollback();
            return Response::json($ex);
        }
    }
}
