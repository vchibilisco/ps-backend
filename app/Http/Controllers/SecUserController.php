<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Profile;
use App\Models\UserProfile;
use Response;
use Input;
use DB;
use Config;

class SecUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = Profile::where('cod','=','GR_SECRE')->first();
        $result = array();
        $users = User::orderBy('dni')->get();
        foreach ($users as $us) {
            $profiles = $us->profiles()->where('profile','=',$profile->id)->get();
            if (sizeof($profiles)>0) {
                array_push($result, $us);
            }
        }
        return Response::json([
            'users' => $result
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        try{
            DB::beginTransaction();
            $input = Input::all();
            $insert['dni'] =  $input['dni'];
            $insert['name'] =  $input['name'];
            $insert['lastname'] =  $input['lastname'];
            $insert['email'] =  $input['email'];
            if(User::where('dni','=',$input['dni'])->first() != null){
                return response()->json(['error' => Config::get('exceptionSar.user_duplicate_dni_unique')], 500);
            }
            if(User::where('email','=',$input['email'])->first() != null){
                return response()->json(['error' => Config::get('exceptionSar.user_duplicate_email_unique')], 500);
            }
            User::create($insert);
            $user = User::where('dni','=',$input['dni'])->first();
            for($i=0; $i < sizeof($input['profile']); $i++) {
                $pr = Profile::where('cod','=',$input['profile'][$i]['cod'])->first();
                $insert['user'] = $user->id;
                $insert['profile'] = $pr->id;
                UserProfile::create($insert);
            }
            DB::commit();
            return Response::json(true);
        }catch(Exception $e){
            DB::rollback();
            if(strpos($e->getMessage(), 'Duplicate entry')){
                return response()->json(['error' => Config::get('exceptionSar.user_duplicate')], 500);
            }else{
                return response()->json(['error' => $e->getMessage()], 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $user['profiles'] = $user->profiles()->get();
        return Response::json([
            'user' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            DB::beginTransaction();
            $input = Input::all();
            $update['dni'] =  $input['dni'];
            $update['name'] =  $input['name'];
            $update['lastname'] =  $input['lastname'];
            $update['email'] =  $input['email'];
            if(User::where('dni','=',$input['dni'])->where('id','<>',$id)->first() != null){
                return response()->json(['error' => Config::get('exceptionSar.user_duplicate_dni_unique')], 500);
            }
            if(User::where('email','=',$input['email'])->where('id','<>',$id)->first() != null){
                return response()->json(['error' => Config::get('exceptionSar.user_duplicate_email_unique')], 500);
            }
            $user = User::find($id);
            $user->update($update);
            $user->profiles()->detach();
            for ($i=0; $i < sizeof($input['profile']); $i++) {
                $pr = Profile::where('cod','=',$input['profile'][$i]['cod'])->first();
                $user->profiles()->attach($pr->id);
            }
            DB::commit();
            return Response::json(true);
        }catch(Exception $e){
            DB::rollback();
            if(strpos($e->getMessage(), 'Duplicate entry')){
                return response()->json(['error' => Config::get('exceptionSar.user_duplicate')], 500);
            }else{
                return response()->json(['error' => $e->getMessage()], 500);
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::beginTransaction();
            $user = User::find($id);
            $user->status = !$user->status;
            $user->save();
            DB::commit();
            return Response::json(true);
        }catch(Exception $ex){
            DB::rollback();
            return Response::json($ex);
        }
    }

    public function SoftDelete(Request $request)
    {
        try{
            DB::beginTransaction();
            $user = User::find($request['userid']);
            $user->status = !$user->status;
            $user->save();
            $user->delete();
            DB::commit();
            return Response::json(true);
        }catch(Exception $ex){
            DB::rollback();
            return Response::json($ex);
        }
    }
}
