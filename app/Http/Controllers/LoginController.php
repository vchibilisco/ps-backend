<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Authentication;
use App\Models\User;
use App\Models\Profile;
use JWTAuth;
use Response;
use Hash;
use Config;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Login(Request $request)
    {
        $section = $request['section'];
        $credentials = $request->only('dni', 'password');
        if($section == 'student'){
          $section = array('GR_STUDS');
        }else{
          $section = array('GR_SECRE','GR_DIRECT','GR_SROOT');
        }
        try {
          $auth = Authentication::where('dni','=',$credentials['dni'])->where('status','=','1')->first();
          if ($auth != null) {
            $user = User::where('dni','=',$auth['dni'])->where('status','=','1')->first();
            if($user != null){
              $profiles = $user->profiles()->get();
              if ($this->inArray($profiles, $section)) {
                $claimUser['id'] = $user['id'];
                $claimUser['dni'] = $user['dni'];
                $claimUser['name'] = $user['name'];
                $claimUser['lastname'] = $user['lastname'];
                $claimUser['email'] = $user['email'];
                $claimUser['profile'] = $this->getArrayProfiels($profiles, $section);
                $claimUser['careers'] = $user->careers()->get();
                $customClaims = ['user' => $claimUser];
                if (! $token = JWTAuth::attempt($credentials,$customClaims)) {
                    return response()->json(['error' => Config::get('exceptionSar.invalid_credentials')], 401);
                }else{
                  return response()->json([
                    'token'=>$token,
                    'profile'=>$claimUser['profile']
                  ]);
                }
              }else{
                return response()->json(['error' => Config::get('exceptionSar.invalid_profiles')], 401);
              }
            }else {
              return response()->json(['error' => Config::get('exceptionSar.invalid_credentials')], 401);   
            }
          }else{
            return response()->json(['error' => Config::get('exceptionSar.user_not_enabled')], 401);
          } 
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => Config::get('exceptionSar.could_not_create_token')], 401);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Logout()
    {
        //
    }

    public function ValidateHash(Request $request){
      try{
        $auth = Authentication::where('hash_enabled','=',$request['hash'])->first();
        if($auth != null){
          return Response::json(true);  
        }else{
          return response()->json(['error' => Config::get('exceptionSar.hash_not_exist')], 500);  
        }
      }catch(Exception $e){
        return response()->json(['error' => Config::get('exceptionSar.could_not_validate_hash')], 500);
      }
    }

    public function UpdatePassword(Request $request)
    {
      try{
        $auth = Authentication::where('hash_enabled','=',$request['hash'])->first();
        if ($auth != null) {
          $user = User::where('dni','=',$auth['dni'])->first();
          $user['status'] = 1;
          $auth['hash_enabled'] = '';
          $auth['password'] = Hash::make($request['password']);
          $auth['status'] = 1;
          $user->save();
          $auth->save();
        }else{
          return response()->json(['error' => Config::get('exceptionSar.hash_not_exist')], 500);  
        }
      }catch(Exception $e){
        return response()->json(['error' => Config::get('exceptionSar.error_update_password')], 500); 
      }
    }

    private function inArray($profiles, $section)
    {
      $result = false;
      foreach ($profiles as $profile) {
        if(in_array($profile->cod, $section)){
          $result = true;
          break;
        }
      }
      return $result;
    }

    private function getArrayProfiels($profiles, $section)
    {
      $result = array();
      if(in_array('GR_STUDS',$section)){
        array_push($result, 'GR_STUDS');
      }else{
        foreach ($profiles as $profile) {
          if($profile->cod != 'GR_STUDS'){
            array_push($result, $profile->cod);
          }
        }
      }
      return $result;
    }
}
