<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\TypeResolution;
use App\Models\AreaResolution;
use Response;
use Input;
use DB;

class TypeResolutionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request['area'] == 'all'){
            $typeResolutions = TypeResolution::orderBy('id','asc')->get();
            $this->getArea($typeResolutions);
            return Response::json([
                'resolutions' => $typeResolutions
            ]);
        }else{
            $typeResolutions = TypeResolution::where('area','=',$request['area'])->orderBy('id','asc')->get();
            $this->getArea($typeResolutions);
            return Response::json([
                'resolutions' => $typeResolutions
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Input::all();
        TypeResolution::create($input);
        return Response::json($input);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tresolution = TypeResolution::find($id);
        return Response::json([
            'resolution' => $tresolution
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = Input::all();
        $result = TypeResolution::find($id)->update($input);
        return Response::json($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::beginTransaction();
            $tresolution = TypeResolution::find($id);
            $result = $tresolution->delete();
            DB::commit();
            return Response::json($result);
        }catch(Exception $e){
            DB::rollback();
            return Response::json($e);
        }
    }

    private function getArea($typeResolutions)
    {
        foreach ($typeResolutions as $tr) {
            $tr['area'] = $tr->area()->first();
        }
    }
}
