1)
alter table rep_resolutions.authenticacion add column hash_enabled varchar (200) null;


2)
CREATE TABLE IF NOT EXISTS `rep_resolutions`.`user_profile` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user` INT(11) NOT NULL,
  `profile` INT(11) NOT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
`created_at` DATETIME NULL DEFAULT NULL
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_user_profile_user1_idx` (`user` ASC),
  INDEX `fk_user_profile_profile1_idx` (`profile` ASC),
  CONSTRAINT `fk_user_profile_user1`
    FOREIGN KEY (`user`)
    REFERENCES `rep_resolutions`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_profile_profile1`
    FOREIGN KEY (`profile`)
    REFERENCES `rep_resolutions`.`profile` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

insert into user_profile  (user, profile)
select u.id, p.id 
from user u
inner join profile p on u.profile = p.id
order by u.id;

ALTER TABLE `rep_resolutions`.`user` 
DROP FOREIGN KEY `fk_user_profile`;

ALTER TABLE `rep_resolutions`.`user` 
DROP COLUMN `profile`,
DROP INDEX `fk_user_profile_idx` ;


3)
INSERT INTO `rep_resolutions`.`area_resolution` (`description`) VALUES ('Secretaría');

4)
ALTER TABLE `rep_resolutions`.`type_resolutions` ADD COLUMN `deleted_at` DATETIME NULL DEFAULT NULL AFTER `status`

ALTER TABLE `rep_resolutions`.`resolutions` ADD COLUMN `deleted_at` DATETIME NULL DEFAULT NULL AFTER `updated_at`;

ALTER TABLE `rep_resolutions`.`career_res` ADD COLUMN `deleted_at` DATETIME NULL DEFAULT NULL AFTER `updated_at`;

ALTER TABLE `rep_resolutions`.`student_res` ADD COLUMN `deleted_at` DATETIME NULL DEFAULT NULL AFTER `updated_at`;

ALTER TABLE `rep_resolutions`.`career` ADD COLUMN `deleted_at` DATETIME NULL DEFAULT NULL AFTER `status`;

ALTER TABLE `rep_resolutions`.`user` ADD COLUMN `deleted_at` DATETIME NULL DEFAULT NULL AFTER `expedient`;


5)
ALTER TABLE `rep_resolutions`.`user` ADD UNIQUE INDEX `email_UNIQUE` (`email` ASC);

ALTER TABLE `rep_resolutions`.`user` DROP INDEX `email_UNIQUE` , DROP INDEX `dni_UNIQUE` ;