'use strict';

fdescribe('Controller: LoginController', function () {

  // load the controller's module
  beforeEach(module('psApp'));

  var controller,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    controller = $controller('LoginController', {
      $scope: scope
    });
  }));
  it('should be exist', function() {
    expect(controller).toBeDefined();
  });
});
