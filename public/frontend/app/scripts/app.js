'use strict';

/**
 * @ngdoc overview
 * @name psApp
 * @description
 * # psApp
 *
 * Main module of the application.
 */;

angular
  .module('psApp', [
    'ngAnimate',
    'ngAria',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'ngFileUpload',
    'angular-jwt',
    'angular-carousel',
    'pdf-viewer'/*,
    'angular-loading-bar'*/
  ])
  .run([
      '$rootScope',
      '$location',
      '$window',
      '$http',
      'AuthService',
      function($rootScope, $location, $window, $http, AuthService){
        var urisWithoutSecurity = [
          '/login',
          '/home'
        ];
        $rootScope.$on('$routeChangeStart', function(event, nextLocation, currentLocation){
          var nextPath = (nextLocation.$$route) ? nextLocation.$$route.originalPath : undefined;
          var currentPath = (currentLocation !== undefined && currentLocation.$$route) ? currentLocation.$$route.originalPath : undefined;
          if(_.contains(urisWithoutSecurity, nextPath)){
            if(currentPath && nextPath === '/login'){
              if (AuthService.isUserLoggedIn()){
                $location.path(currentPath);
                return;
              }
            }
            $location.path(nextPath);
            return;
          }
          if(AuthService.isUserLoggedIn()){
            $location.path(nextPath || '/dashboard');
          }else{
            $location.path('/home');
          }
        });
      }
    ]);

angular.module('psApp').config([
  '$httpProvider',
  function($httpProvider) {
    $httpProvider.interceptors.push(SessionInjectorService);
  }
]);