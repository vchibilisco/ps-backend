'use strict';

angular.module('psApp').factory('ReportsResource', function($resource,Config) {
  return $resource(
    Config.APPURL+'/api/reports/:id', 
    null,
    {
      'query': {method: 'GET'}
    }
  );
});