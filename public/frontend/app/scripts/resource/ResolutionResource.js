'use strict';

angular.module('psApp').factory('ResolutionResource', function($resource,Config) {
  return $resource(
    Config.APPURL+'/api/resolution/:id', 
    null,
    {
      'query': {method: 'GET'},
      'save': {method: 'POST'},
      'get': {method: 'GET', params: {id:'@id'}, isArray: false},
      'update': {method:'PUT'},
      'remove': {method:'DELETE'},
      'removeCareerRes': {method:'GET', params: {section:'@section',id:'@id'}}
    }
  );
});