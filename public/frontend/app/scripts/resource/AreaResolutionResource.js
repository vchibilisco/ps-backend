'use strict';

angular.module('psApp').factory('AreaResolutionResource', function($resource,Config) {
  return $resource(
    Config.APPURL+'/api/arearesolution/:id', 
    null,
    {
      'query': {method: 'GET'}
    }
  );
});