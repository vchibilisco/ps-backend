'use strict';

angular.module('psApp').factory('StudUserResource', function($resource,Config) {
  return $resource(
    Config.APPURL+'/api/studUser/:id', 
    null,
    {
      'query': {method: 'GET'},
      'save': {method: 'POST'},
      'get': {method: 'GET', params: {id:'@id'}, isArray: false},
      'update': {method:'PUT'},
      'remove': {method:'DELETE'}
    }
  );
});