'use strict';

angular.module('psApp').factory('ProfileResource', function($resource,Config) {
  return $resource(
    Config.APPURL+'/api/profile', 
    null,
    {
      'query': {method: "GET"}
    }
  );
});