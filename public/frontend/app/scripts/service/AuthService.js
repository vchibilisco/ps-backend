'use strict';

var AuthService = function(
	$window,
	jwtHelper
) {
  return{
		IsSecre: function() {
			return $window.sessionStorage['Profile'].includes('GR_SECRE');
		},
		IsDirect: function() {
			return $window.sessionStorage['Profile'].includes('GR_DIRECT');
		},
		IsStud: function() {
			return $window.sessionStorage['Profile'].includes('GR_STUDS');
		},
		IsAdmin: function() {
			return $window.sessionStorage['Profile'].includes('GR_SROOT');
		},
		GetToken: function() {
			return $window.sessionStorage.Authorization;
		},
		Clear: function() {
			delete $window.sessionStorage.Authorization;
		},
		isUserLoggedIn: function () {
			var tk = $window.sessionStorage.Authorization;
			return !(typeof tk === 'undefined' || tk === '' || tk === 'undefined');
		},
		getPayload: function() {
			return jwtHelper.decodeToken($window.sessionStorage.Authorization);
		}
  }
};

angular.module('psApp').factory('AuthService', AuthService, [
	'$window',
	'jwtHelper'
]);