'use strict';

var SessionInjectorService = function(
  $window, 
  $location,
  $q,
  $rootScope,
  jwtHelper
) {
	var auth = {};
  return{
		response: function(res) {
      if (res.config.url.indexOf('/api/authentication') > 0) {
        $window.sessionStorage['Authorization'] = res.data.token;
        $window.sessionStorage['Profile'] = res.data.profile;
        var payload = jwtHelper.decodeToken($window.sessionStorage['Authorization']);
        $rootScope.user = payload.user;
        $rootScope.user.IsLogin = true;
      }
      if(res.data.error === 'token_not_provided' || res.data.error === 'token_expired' || res.data.error === 'token_invalid'){
        $window.sessionStorage.clear();
        $location.path('/home');
      }
      return res;
    },
    request: function(req) {
      if (req.url.indexOf('/api/authentication') < 0) {
        req.headers.Authorization = 'Bearer ' + $window.sessionStorage['Authorization'];
      }
      return req;
    },
    requestError: function(reqError) {
      return reqError;
    },
    responseError: function(resError) {
      if (resError.data.error === 'token_invalid') {
        $window.sessionStorage.clear();
        $location.path('/home');
      }
      return $q.reject(resError);
    }
	}
};

angular.module('psApp').factory('SessionInjectorService', SessionInjectorService, 
  [
    '$window',
    '$location',
    '$q',
    '$rootScope',
    'jwtHelper'
  ]);