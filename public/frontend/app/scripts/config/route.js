'use strict';

angular.module('psApp').config([
	'$routeProvider',
	function ($routeProvider) {
    $routeProvider
      .when('/home', {
        templateUrl: 'frontend/app/views/home/home.html',
        controller: 'HomeController'
      })
      .when('/viewer', {
        templateUrl: 'frontend/app/views/common/viewer/viewer.html',
        controller: 'ViewerController'
      })
      .when('/dashboard', {
        templateUrl: 'frontend/app/views/home/dashboard.html',
        controller: 'DashboardController'
      })
      .when('/students-user', {
        templateUrl: 'frontend/app/views/students/students.html',
        controller: 'StudentsController'
      })
      .when('/secretaryship-user', {
        templateUrl: 'frontend/app/views/secretaryship-user/list.html',
        controller: 'SecretaryshipController'
      })
      .when('/careers-user', {
        templateUrl: 'frontend/app/views/careers-user/list.html',
        controller: 'CareerUserController'
      })
      .when('/careers-res', {
        templateUrl: 'frontend/app/views/careers-res/main.html',
        controller: 'CareerResController'
      })
      .when('/secretaryship-res', {
        templateUrl: 'frontend/app/views/secretaryship-res/list.html',
        controller: 'SecretaryshipResConstroller'
      })
      .when('/students-res', {
        templateUrl: 'frontend/app/views/student-res/main.html',
        controller: 'StudentsResController'
      })
      .when('/profiles-conf', {
        templateUrl: 'frontend/app/views/profiles/list.html',
        controller: 'ProfileController'
      })
      .when('/careers-conf', {
        templateUrl: 'frontend/app/views/careers-conf/list.html',
        controller: 'CareerController'
      })
      .when('/resolutions-conf', {
        templateUrl: 'frontend/app/views/configuration/resolutions/list.html',
        controller: 'ConfResolutionController'
      })
      .when('/reports', {
        templateUrl: 'frontend/app/views/reports/reports.html',
      })
      .when('/', {
        redirectTo: '/home'
      })
      .otherwise({
        redirectTo: '/home'
      });
  }
]);