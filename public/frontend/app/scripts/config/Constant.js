'use strict';

angular.module('psApp').constant('Config',{
	'APPURL':'http://ps.postgrado.com'
});

angular.module('psApp').constant('Views',{
	'CONFIRMATION':{
		'disablemodal':'frontend/app/views/message/disabledConfirmation.html',
		'deletemodal':'frontend/app/views/message/deleteConfirmation.html',
		'enabledUser':'frontend/app/views/message/enabledUserConfirmation.html',
		'notresolution':'frontend/app/views/message/notresolution.html',
		'controller':'MessageConfirmationController'
	},
	'STUDENT_MODULE':{
		'modal':'frontend/app/views/students/modal/addStudent.html',
		'controllermodal':'ModalAddStudentController'
	},
	'CAREER_MODULE':{
		'modal_conf':'frontend/app/views/careers-conf/modal.html',
		'controllermodal_conf':'ModalCareerController',
		'modal_user':'frontend/app/views/careers-user/modal.html',
		'controllermodal_user':'ModalCareerUserController'
	},
	'SECRETARYSHIP_MODULE':{
		'modal':'frontend/app/views/secretaryship-user/modal.html',
		'controllermodal':'ModalSecretaryshipController'
	},
	'RESOLUTION_MODULE':{
		'modal_conf':'frontend/app/views/configuration/resolutions/modal/modal.html',
		'controllermodal_conf':'ModalAddTypeResolutionController'
	},
	'UPLOAD_RESOLUTION':{
		'modal_res':'frontend/app/views/common/updaload-file/modal.html',
		'controllermodal_res':'ModalUpdaloadResolutionController'
	}
});