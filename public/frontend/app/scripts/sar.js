$(document).ready(function(){
  var dropup = function() {
    $(".dropdown-toggle").each(function(){ 
      var par=$(this).parents('.btn-group');
      var dropl=par.find('ul');
      var otop=$(this).offset().top+$(this).height()-$(window).scrollTop();
      var ulh=dropl.height();
      var obot=$(window).height()-$(this).height()-$(this).offset().top+$(window).scrollTop();

      if ((obot < ulh+10) && (otop > ulh+10)) {
        par.addClass('dropup');
      } else {
        par.removeClass('dropup');
      }

    });
  } 

  $(window).load(dropup);
  $(window).bind('resize scroll touchstart touchmove mousewheel click', dropup);
});