'use strict';

var ModalAddStudentController = function(
	$scope, 
	$uibModalInstance,
	current,
	CareerResource,
	StudUserResource,
	ProfileResource
) {
	$scope.careers = [];
	$scope.data = {};
	$scope.hasError = false;
	$scope.action = 'add';
	$scope.title = 'Nuevo';
	$scope.usercareers = [];
	$scope.arraySelectedProfiles = [];
	$scope.alerts = [];
	CareerResource.query(function(response){
		$scope.careers = response.careers;
	},function(error) {
		console.log(error);
	});

	$scope.Add = function (form) {
		if(form.$valid){
			var request = {
				expedient: $scope.expedient,
				dni: $scope.dni,
				name: $scope.name,
				lastname: $scope.lastname,
				email: $scope.email,
				address: $scope.address,
				phone: $scope.phone,
				careers: $scope.usercareers,
				profile: $scope.arraySelectedProfiles
			}
			if($scope.action === 'add'){
				StudUserResource.save(request, function(result) {
					$uibModalInstance.close(true);
				},function(error) {
					$scope.alerts = [{msg: error.data.error}];
				});
			}else if($scope.action === 'edit'){
			  StudUserResource.update({id:current.id},request,function(response) {
			  	$uibModalInstance.close(true);
			  },function(error) {
			  	$scope.alerts = [{msg: error.data.error}];
			  });
			}
		}else{
			$scope.hasError = true;
		}
	};

	$scope.Cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

	$scope.AddCareer = function() {
		var node = {
			career: '',
			status: '1'
		}
		$scope.usercareers.push(node);
	};

	$scope.DeleteCareer = function(index) {
		$scope.usercareers.splice(index, 1);
	};

	if(current !== undefined){
		$scope.usercareers = current.careers;
		for(var i = 0; i < $scope.usercareers.length; i++){
			$scope.usercareers[i].id = String($scope.usercareers[i].id);
		}
		$scope.arraySelectedProfiles = current.profiles;
		$scope.action = 'edit';
		$scope.title = 'Modificar';
		$scope.expedient = current.expedient;
		$scope.dni= current.dni;
		$scope.name= current.name;
		$scope.lastname= current.lastname;
		$scope.email= current.email;
		$scope.address= current.address;
		$scope.phone= current.phone;
	}

	ProfileResource.get(function(response) {
		$scope.profiles = response.profiles;
	},function(error) {
		console.log(error)
	});

	if($scope.usercareers.length === 0){
		$scope.AddCareer();
	}

	$scope.AddProfile = function() {
		var node = {
			cod: ''
		}
		$scope.arraySelectedProfiles.push(node);
	};

	$scope.DeleteProfile = function(index) {
		$scope.arraySelectedProfiles.splice(index, 1);
	};

	if($scope.arraySelectedProfiles.length === 0){
		$scope.arraySelectedProfiles.push({cod:'GR_STUDS'});
	}

	$scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };
};

angular.module('psApp').controller('ModalAddStudentController', ModalAddStudentController,
	[
		'$scope',
		'$uibModalInstance',
		'current',
		'CareerResource',
		'StudUserResource',
		'ProfileResource'
	]);