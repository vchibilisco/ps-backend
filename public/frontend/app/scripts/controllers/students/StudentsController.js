'use strict';

var StudentsController = function(
	$uibModal,
	$http,
	$window,
	StudUserResource,
	Views,
	Config
) {
	var that = this;
	this.users = [];
  this.alerts = [];
	var GetUsers = function() {
		StudUserResource.query(function(response) {
			that.users = response.users;
		},function(error) {
			console.log(error)
		});
	};
	this.OpenModal = function(action, id){
		switch(action){
			case 'add':
				Open(undefined);
				break;
			case 'edit':
				StudUserResource.get({id:id},function(response) {
					Open(response.user);
				},function(error) {
					console.log(error);
				});
				break;
			case 'drop':
				Delete(id);
				break;
      case 'softdrop':
        SoftDelete(id);
        break;
		};
	};
	var Open = function(currentObject) {
		var modalInstance = $uibModal.open({
      animation: true,
      backdrop: 'static',
      templateUrl: Views.STUDENT_MODULE.modal,
      controller: Views.STUDENT_MODULE.controllermodal,
      resolve: {
        current: function () {
          return currentObject;
        }
      }
    });

    modalInstance.result.then(function (result) {
    	GetUsers();
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
	};	
	var Delete = function(id){
		var modalInstance = $uibModal.open({
      animation: true,
      backdrop: 'static',
      templateUrl: Views.CONFIRMATION.disablemodal,
      controller: Views.CONFIRMATION.controller
    });

    modalInstance.result.then(function (result) {
      if(result){
      	StudUserResource.delete({id:id},function(response) {
      		GetUsers();
      	},function(error) {
      		console.log(error);
      	});
      }
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
	};

  var SoftDelete = function(id) {
    var modalInstance = $uibModal.open({
      animation: true,
      backdrop: 'static',
      templateUrl: Views.CONFIRMATION.deletemodal,
      controller: Views.CONFIRMATION.controller,
    });

    modalInstance.result.then(function (result) {
      if(result){
        var request = {
          'userid': id
        };
        $http({
          method: 'POST',
          url: Config.APPURL+'/api/softdeletestd',
          data: $.param(request),
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization':'Bearer ' + $window.sessionStorage['Authorization']
          }
        }).success(function(response) {
          that.alerts = [{type:'success', msg:'Se eliminó al Usuario correctamente.'}];
          GetUsers();
        }).error(function(error) {
          that.alerts = [{type:'danger', msg:'Ocurrio un error y no se pudo completar el proceso.'}];
        });
      }
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

	this.EnableUser = function(user){
    var userid = {
      userid: user.id
    };
    var modalInstance = $uibModal.open({
      animation: true,
      backdrop: 'static',
      templateUrl: Views.CONFIRMATION.enabledUser,
      controller: Views.CONFIRMATION.controller
    });

    modalInstance.result.then(function (result) {
      if(result){
        $http({
          method: 'POST',
          url: Config.APPURL+'/api/sendemail',
          data: $.param(userid),
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization':'Bearer ' + $window.sessionStorage['Authorization']
          }
        }).success(function(response) {
          that.alerts = [{type:'success', msg:'Se envió el email al usuario.'}];
          GetUsers();
        }).error(function(error) {
          that.alerts = [{type:'danger', msg:'Ocurrio un error y no se pudo completar el proceso.'}];
        });
      }
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  this.closeAlert = function(index) {
    this.alerts.splice(index, 1);
  };

	GetUsers();
};

angular.module('psApp').controller('StudentsController', StudentsController,
	[
		'$uibModal',
		'$http',
		'$window',
		'StudUserResource',
		'Views',
		'Config'
	]);