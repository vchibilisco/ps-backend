'use strict';

var SidebarController = function(
	$location, 
	$rootScope, 
	AuthService
) {
	this.urlview = '';
	this.url = '';
	this.IsSecre = function() {
		return AuthService.IsSecre();
	};
	this.IsDirect = function() {
		return AuthService.IsDirect();
	};
	this.IsStud = function() {
		return AuthService.IsStud();
	};
	this.IsAdmin = function() {
		return AuthService.IsAdmin();
	};
	this.IsLogin = function() {
		return AuthService.isUserLoggedIn();
	}
	this.Logout = function() {
		AuthService.Clear();
		//$rootScope.user.IsLogin = false;
		this.ChangeLocation('/home');
	}
	this.ChangeLocation = function(url, urlview) {
		this.urlview = urlview;
		this.url = url;
		$location.path(url);
	};
	this.GetName = function() {
		var payload =AuthService.getPayload();
		return payload.user.lastname + ' ' + payload.user.name;
	};
};

angular.module('psApp').controller('SidebarController', SidebarController, 
	[
		'$location',
		'$rootScope',
		'AuthService'
	]);