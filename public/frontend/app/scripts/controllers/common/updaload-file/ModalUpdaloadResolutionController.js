'use strict';

var ModalUpdaloadResolutionController = function(
		$scope, 
		$uibModalInstance,
		$window,
		Upload,
		parent,
		TypeResolutionResource,
		ResolutionResource,
		Config
	) 
{
	$scope.hasError = false;
	$scope.action = 'add';
	$scope.typeresolutions = [];
	$scope.loading = false;
	$scope.danger = false;
	var reqTypRes = {
		area: (parent.section==='career-res')
					? 1 
					: (
							(parent.section==='student-res')
							? 2 
							: (parent.section==='secre-res')
							? 3
							: 'all')
	}
	TypeResolutionResource.query(reqTypRes,function(result) {
		$scope.typeresolutions = result.resolutions;
	}, function(error) {
		console.log(error)
	});
	$scope.popup = {
    opened: false
  };
	$scope.today = function() {
    $scope.dt = new Date();
  };
	$scope.Add = function (form) {
		if (form.$valid) {
			var request = {
				nresolution:$scope.nresolution,
				description:$scope.description,
				tresolution: $scope.tresolution,
				dateresolution: $scope.dt,
				parent: parent
			};
			$scope.danger = false;
			$scope.loading = true;
			Upload.upload({
          url: Config.APPURL+'/api/resolution',
          data: {fileres:$scope.fileres, request: request},
          headers: {'Authorization':'Bearer ' + $window.sessionStorage['Authorization']}
      }).progress(function (evt) {
          /*var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);*/
      }).success(function (data, status, headers, config) {
      	$uibModalInstance.close(true);
      	$scope.loading = false;
      }).catch(function(error) {
      	$scope.loading = false;
      	$scope.danger = true;
      });
		} else {
			$scope.hasError = true;
		}
		
	};
	$scope.Cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
	$scope.openDP = function(){
		$scope.popup.opened = true;
	};
	function disabled(data) {
    var date = data.date,
    mode = data.mode;
    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
  }
	$scope.dateOptions = {
    formatYear: 'yy'
  };
  $scope.altInputFormats = ['M!/d!/yyyy'];

	$scope.today();
};

angular.module('psApp').controller('ModalUpdaloadResolutionController', ModalUpdaloadResolutionController,
	[
		'$scope',
		'$uibModalInstance',
		'$window',
		'Upload',
		'parent',
		'TypeResolutionResource',
		'ResolutionResource',
		'Config'
	]);