'use strict';

var ViewerController = function(
		HelpService
	) 
{
	var _this = this;
	_this.urlPdf = HelpService.data;
};

angular.module('psApp').controller('ViewerController', ViewerController,
	[
		'HelpService'
	]);