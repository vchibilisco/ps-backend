'use strict';

var ProfileController = function(ProfileResource) {
	this.profiles = [];
	var that = this;
	var profilesResource = new ProfileResource();
	profilesResource.$query().then(function(response) {
    that.profiles = response.profiles;
  })
  .catch(function(error) {
    console.log(error);
  });
};

angular.module('psApp').controller('ProfileController', ProfileController,['ProfileResource']);