'use strict';

var ModalSecretaryshipController = function(
		$scope, 
		$uibModalInstance, 
		SecUserResource, 
		ProfileResource, 
		current
	) 
{
	$scope.hasError = false;
	$scope.action = 'add';
	$scope.title = 'Nuevo';
	$scope.hasError = false;
	$scope.arraySelectedProfiles = [];
	$scope.profiles = [];
	$scope.data = [];
	$scope.alerts = [];
	if (current !== undefined) {
		$scope.action = 'edit';
		$scope.title = 'Modificar';
		$scope.dni = current.dni;
		$scope.name = current.name;
		$scope.lastname = current.lastname;
		$scope.email = current.email;
		$scope.arraySelectedProfiles = current.profiles;
	}
	ProfileResource.get(function(response) {
		$scope.profiles = response.profiles;
	},function(error) {
		console.log(error)
	});
	
	$scope.Add = function (form) {
		if (form.$valid) {
			var request = {
				dni: $scope.dni,
				name: $scope.name,
				lastname: $scope.lastname,
				email: $scope.email,
				profile: $scope.arraySelectedProfiles
			};

			if ($scope.action === 'add') {
				SecUserResource.save(request,function(response) {
					$uibModalInstance.close(true);
				},function(error) {
					$scope.alerts = [{msg: error.data.error}];
				});
			} else if($scope.action === 'edit'){
			  SecUserResource.update({id:current.id},request,function(response) {
			  	$uibModalInstance.close(true);
			  },function(error) {
			  	$scope.alerts = [{msg: error.data.error}];
			  });
			}	
		} else {
			$scope.hasError = true;
		}
		
	};
	$scope.Cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
	$scope.AddProfile = function() {
		var node = {
			cod: ''
		}
		$scope.arraySelectedProfiles.push(node);
	};
	$scope.DeleteProfile = function(index) {
		$scope.arraySelectedProfiles.splice(index, 1);
	};
	if($scope.arraySelectedProfiles.length === 0){
		$scope.arraySelectedProfiles.push({cod:'GR_SECRE'});
	}
	$scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };
};

angular.module('psApp').controller('ModalSecretaryshipController', ModalSecretaryshipController,
	[
		'$scope',
		'$uibModalInstance',
		'SecUserResource',
		'ProfileResource',
		'current'
	]);