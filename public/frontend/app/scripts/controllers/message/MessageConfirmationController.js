'use strict';

var MessageConfirmationController = function($scope, $uibModalInstance) {
	
	$scope.Ok = function (form) {
    $uibModalInstance.close(true);
	};
	$scope.Cancel = function () {
		$uibModalInstance.dismiss('cancel');
	}
};

angular.module('psApp').controller('MessageConfirmationController', MessageConfirmationController,['$scope','$uibModalInstance']);