'use strict';

var ModalCareerUserController = function(
	$scope,
	$uibModalInstance,
	CareerUserResource,
	ProfileResource,
	CareerResource,
	current)
{
	$scope.hasError = false;
	$scope.action = 'add';
	$scope.title = 'Nuevo';
	$scope.hasError = false;
	$scope.data = [];
	$scope.careers = [];
	$scope.usercareers = [];
	$scope.arraySelectedProfiles = [];
	$scope.alerts = [];
	var GetCareers = function() {
		CareerResource.query(function(result) {
			$scope.careers = result.careers;
		},function(error) {
			console.log(error);
		});
	};
	
	$scope.Add = function (form) {
		if (form.$valid) {
			var request = {
				dni: $scope.dni,
				name: $scope.name,
				lastname: $scope.lastname,
				email: $scope.email,
				careers: $scope.usercareers,
				profile: $scope.arraySelectedProfiles
			};
			if ($scope.action === 'add') {
				CareerUserResource.save(request,function(response) {
					$uibModalInstance.close(true);
				},function(error) {
					$scope.alerts = [{msg: error.data.error}];
				});
			} else if($scope.action === 'edit'){
			  CareerUserResource.update({id:current.id},request,function(response) {
			  	$uibModalInstance.close(true);
			  },function(error) {
			  	$scope.alerts = [{msg: error.data.error}];
			  });
			}	
		} else {
			$scope.hasError = true;
		}
		
	};
	
	$scope.Cancel = function () {
		$uibModalInstance.dismiss('cancel');
	}

	$scope.AddCareer = function() {
		var node = {
			career: '',
			status: '1'
		}
		$scope.usercareers.push(node);
	};

	$scope.DeleteCareer = function(index) {
		$scope.usercareers.splice(index, 1);
	};

	if($scope.usercareers.length === 0){
		$scope.AddCareer();
	}

	if (current !== undefined) {
		$scope.usercareers = current.careers;
		for(var i = 0; i < $scope.usercareers.length; i++){
			$scope.usercareers[i].id = String($scope.usercareers[i].id);
		}
		$scope.action = 'edit';
		$scope.title = 'Modificar';
		$scope.dni = current.dni;
		$scope.name = current.name;
		$scope.lastname = current.lastname;
		$scope.email = current.email;
		$scope.arraySelectedProfiles = current.profiles;
	}else{
		GetCareers();
	}
	ProfileResource.get(function(response) {
			$scope.profiles = response.profiles;
		},function(error) {
			console.log(error)
		});
	if($scope.usercareers.length === 0){
		$scope.AddCareer();
	}
	$scope.AddProfile = function() {
		var node = {
			cod: ''
		}
		$scope.arraySelectedProfiles.push(node);
	};
	$scope.DeleteProfile = function(index) {
		$scope.arraySelectedProfiles.splice(index, 1);
	};
	if($scope.arraySelectedProfiles.length === 0){
		$scope.arraySelectedProfiles.push({cod:'GR_DIRECT'});
	};
	GetCareers();
	$scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };
};

angular.module('psApp').controller('ModalCareerUserController', ModalCareerUserController,
	[
		'$scope',
		'$uibModalInstance',
		'CareerUserResource',
		'ProfileResource',
		'CareerResource',
		'current'
	]);