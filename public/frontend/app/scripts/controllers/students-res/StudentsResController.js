'use strict';

var StudentsResController = function(
	$uibModal,
	$http,
	$window,
	AuthService,
	StudUserResource,
	ResolutionResource,
	Views,
	Config
) {
	var that = this;
	this.isStud = false;
	this.isDir = false;
	this.page = 'main';
	this.currentStudent = undefined;
	this.alerts = [];
	this.showBtnNotfAddRes = true;
	this.showBtnActions = true;
	this.showBtnBack = true;
	var GetStudents = function() {
		StudUserResource.query(function(result) {
			that.students = result.users;
			if(AuthService.IsStud()&&!AuthService.IsSecre()&&!AuthService.IsAdmin()){
				that.isStud = true;
				that.GetResolution(that.students[0]);
			}else if(AuthService.IsDirect()){
				that.isDir = true;
			}
			if(that.isDir){
				that.showBtnNotfAddRes = false;
				that.showBtnActions = false;
			}
			if(that.isStud){
				that.showBtnNotfAddRes = false;
				that.showBtnBack = false;
			}
		},function(error) {
			console.log(error);
		});
	};
	this.GetResolution = function(student) {
		ResolutionResource.query({section:'student-res', idstudent:student.id},function(response) {
      that.resolutions = response.resolutions;
      that.page = 'resolution';
      that.currentStudent = student;
    },function(error) {
      console.log(error);
    });
	};
	this.GoBack = function (partialPage) {
		this.page = partialPage;
	};
	this.OpenModal = function(action, id){
		switch(action){
			case 'add':
				Open();
				break;
			case 'drop':
        Delete(id);
				break;
		}
	};

	var Open = function() {
		var modalInstance = $uibModal.open({
      animation: true,
      backdrop: 'static',
      templateUrl: Views.UPLOAD_RESOLUTION.modal_res,
      controller: Views.UPLOAD_RESOLUTION.controllermodal_res,
      resolve: {
        parent: function () {
          return {section:'student-res', idstudent:that.currentStudent.id};
        }
      }
    });

    modalInstance.result.then(function (result) {
    	that.GetResolution(that.currentStudent);
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
	};

	var Delete = function(id) {
  	var modalInstance = $uibModal.open({
      animation: true,
      backdrop: 'static',
      templateUrl: Views.CONFIRMATION.deletemodal,
      controller: Views.CONFIRMATION.controller,
    });

    modalInstance.result.then(function (result) {
      if(result){
      	var request = {
        	section: 'career-res',
        	id: id
        };
        $http({
		      method: 'POST',
		      url: Config.APPURL+'/api/dropuserres',
		      data: $.param(request),
		      headers: {
		      	'Content-Type': 'application/x-www-form-urlencoded',
		      	'Authorization':'Bearer ' + $window.sessionStorage['Authorization']
		      }
			  }).success(function(response) {
			    that.GetResolution(that.currentStudent);
			  }).error(function(error) {
			  	alert('Error');
			  });
      }
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  this.Notification = function() {
  	var userid = {
  		userid: this.currentStudent.id
  	};
		var modalInstance = $uibModal.open({
      animation: true,
      backdrop: 'static',
      templateUrl: Views.CONFIRMATION.notresolution,
      controller: Views.CONFIRMATION.controller,
    });

    modalInstance.result.then(function (result) {
      if(result){
      	$http({
		      method: 'POST',
		      url: Config.APPURL+'/api/notificationresolution',
		      data: $.param(userid),
		      headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
		      	'Authorization':'Bearer ' + $window.sessionStorage['Authorization']
		      }
			  }).success(function() {
			    that.alerts = [{type:'success', msg:'Se envió el email al usuario.'}];
			  }).error(function(error) {
			  	that.alerts = [
			  		{type:'danger', msg: error.error}
			  	];
			  });
      }
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  this.closeAlert = function(index) {
    this.alerts.splice(index, 1);
  };

	GetStudents();
};

angular.module('psApp').controller('StudentsResController', StudentsResController, [
	'$uibModal',
	'$http',
	'$window',
	'AuthService',
	'StudUserResource',
	'ResolutionResource',
	'Views',
	'Config'
]);