'use strict';

/**
 * @ngdoc function
 * @name psApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the psApp
 */
angular.module('psApp').controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
