'use strict';

var CareerResController = function(
	$uibModal,
	$http,
	$window,
	AuthService,
	CareerResource,
	ResolutionResource,
	Views,
	Config
	)
{
	var that = this;
	this.careers = [];
	this.isDirect = false;
	this.currentCareer = undefined;
	this.alerts = [];
	var GetCareers = function() {
		CareerResource.query(function(response) {
			that.careers = response.careers;
			that.page = 'main';
			if(AuthService.IsDirect()&&!AuthService.IsSecre()&&!AuthService.IsStud()&&!AuthService.IsAdmin()){
				that.isDirect = true;
			}
		},function(error) {
			console.log(error);
		});
	};
  this.OpenModal = function(action, id){
		switch(action){
			case 'add':
				Open();
				break;
			case 'drop':
        Delete(id);
				break;
		}
	};

	var Open = function() {
		var modalInstance = $uibModal.open({
      animation: true,
      backdrop: 'static',
      templateUrl: Views.UPLOAD_RESOLUTION.modal_res,
      controller: Views.UPLOAD_RESOLUTION.controllermodal_res,
      resolve: {
        parent: function () {
          return {section:'career-res', idcareer:that.currentCareer.id};
        }
      }
    });

    modalInstance.result.then(function (result) {
    	that.GetResolution(that.currentCareer);
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
	};

	var Delete = function(id) {
  	var modalInstance = $uibModal.open({
      animation: true,
      backdrop: 'static',
      templateUrl: Views.CONFIRMATION.deletemodal,
      controller: Views.CONFIRMATION.controller,
    });

    modalInstance.result.then(function (result) {
      if(result){
      	var request = {
        	section: 'career-res',
        	id: id
        };
        $http({
		      method: 'POST',
		      url: Config.APPURL+'/api/dropcareerres',
		      data: $.param(request),
		      headers: {
		      	'Content-Type': 'application/x-www-form-urlencoded',
		      	'Authorization':'Bearer ' + $window.sessionStorage['Authorization']
		      }
			  }).success(function(response) {
			    that.GetResolution(that.currentCareer);
			  }).error(function(error) {
			  	alert('Error');
			  });
      }
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  this.GetResolution = function(selectCarrer) {
  	ResolutionResource.query({section:'career-res', idcareer:selectCarrer.id},function(response) {
      that.resolutions = response.resolutions;
      that.page = 'resolutions';
      that.currentCareer = selectCarrer;
    },function(error) {
      console.log(error);
    });
  };

  this.GoBack = function () {
		GetCareers();
	};

	this.Notification = function() {
  	var request = {
  		career: this.currentCareer.id
  	};
		var modalInstance = $uibModal.open({
      animation: true,
      backdrop: 'static',
      templateUrl: Views.CONFIRMATION.notresolution,
      controller: Views.CONFIRMATION.controller,
    });

    modalInstance.result.then(function (result) {
      if(result){
      	$http({
		      method: 'POST',
		      url: Config.APPURL+'/api/notrescareer',
		      data: $.param(request),
		      headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
		      	'Authorization':'Bearer ' + $window.sessionStorage['Authorization']
		      }
			  }).success(function() {
			    that.alerts = [{type:'success', msg:'Se envió el email a los usuarios.'}];
			  }).error(function(error) {
			  	that.alerts = [
			  		{type:'danger', msg: error.error}
			  	];
			  });
      }
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  this.closeAlert = function(index) {
    this.alerts.splice(index, 1);
  };

	GetCareers();
};

angular.module('psApp').controller('CareerResController', CareerResController,
	[
		'$uibModal',
		'$http',
		'$window',
		'AuthService',
		'CareerResource',
		'ResolutionResource',
		'Views',
		'Config'
	]);