'use strict';

var ModalAddTypeResolutionController = function(
	$scope, 
	$uibModalInstance, 
	current, 
	TypeResolutionResource,
	AreaResolutionResource
) {
	$scope.title = 'Nuevo';
	var action = 'add';
	$scope.hasError = false;
	$scope.areas = [];
	if(current !== undefined){
		$scope.description = current.description;
		$scope.area = current.area;
		action = 'edit';
		$scope.title = 'Modificar';
	}
	AreaResolutionResource.query(function(response) {
		$scope.areas = response.arearesolutions;
	});

	$scope.Add = function (form) {
		if(form.$valid){
			var request = {
				description: $scope.description,
				area: $scope.area
			};
			if(action === 'add'){
				TypeResolutionResource.save(request, function(response) {
					$uibModalInstance.close(true);
				},function(error) {
					console.log(error);
				});
			}else if(action === 'edit'){
				TypeResolutionResource.update({id:current.id},request, function(response) {
					$uibModalInstance.close(true);
				},function(error) {
					console.log(error);
				});
			}
		}else{
			$scope.hasError = true;
		}
	};
	$scope.Cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
};

angular.module('psApp').controller('ModalAddTypeResolutionController', ModalAddTypeResolutionController,
	[
		'$scope',
		'$uibModalInstance',
		'curent',
		'TypeResolutionResource',
		'AreaResolutionResource'
	]);