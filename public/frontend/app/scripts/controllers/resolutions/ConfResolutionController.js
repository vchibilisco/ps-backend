'use strict';

var ConfResolutionController = function(
  $uibModal, 
  TypeResolutionResource,
  Views
) {
	this.resolutions = [];
  this.alerts = [];
	var that = this;
	var GetResolutions = function() {
    var request = {area:'all'};
		TypeResolutionResource.query(request,function(result) {
			that.resolutions = result.resolutions;
		},function(error) {
			console.log(error);
		});
	};

	this.OpenModal = function(action, id){
		switch(action){
			case 'add':
				Open(undefined);
				break;
			case 'edit':
				TypeResolutionResource.get({id:id},function(response) {
					Open(response.resolution);
				},function(error) {
					console.log(error);
				});
				break;
			case 'drop':
				Delete(id);
				break;
		};
	}

	var Open = function(currentObject) {
		var modalInstance = $uibModal.open({
      animation: true,
      backdrop: 'static',
      templateUrl: Views.RESOLUTION_MODULE.modal_conf,
      controller: Views.RESOLUTION_MODULE.controllermodal_conf,
      resolve: {
      	current : function() {
      		return currentObject;
      	}
      }
    });

    modalInstance.result.then(function(result) {
    	if(result)
    		GetResolutions();
    });
	};

	var Delete = function(id) {
  	var modalInstance = $uibModal.open({
      animation: true,
      backdrop: 'static',
      templateUrl: Views.CONFIRMATION.deletemodal,
      controller: Views.CONFIRMATION.controller,
    });

    modalInstance.result.then(function (result) {
      if(result){
      	TypeResolutionResource.delete({id:id}, function() {
          that.alerts = [{type:'success', msg:'Se eliminó registro correctamente.'}];
      		GetResolutions();
      	}, function(error) {
      		that.alerts = [{type:'danger', msg:'Ocurrio un error y no se pudo completar el proceso.'}];
      	});
      }
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  this.closeAlert = function(index) {
    this.alerts.splice(index, 1);
  };

	GetResolutions();
};

angular.module('psApp').controller('ConfResolutionController', ConfResolutionController,
  [
    '$uibModal',
    'TypeResolutionResource',
    'Views'
  ]);