'use strict';

var ResetPassController = function(
	$http,
	$location,
	Config) {
	var _this = this;
	_this.hasError = false;
	_this.regex = /(?=^.{6,}$).*$/;
	_this.Submit = function(form, hash){
		if (form.$valid) {
			if (_this.password === _this.repassword) {
				var request = {
					hash: hash,
					password: _this.password
				};
				$http({
		      method: 'POST',
		      url: Config.APPURL+'/api/updatepassword',
		      data: $.param(request),
		      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			  }).success(function(response) {
			   	delete $location.$$search.resetpass;
					$location.path('/home');
			  }).error(function(response) {
			  	_this.error = response.error;
			  });
			}else{
				_this.error = 'Los dos campos del formulario deben ser iguales.';
				_this.hasError = true;
			}
		} else {
			_this.hasError = true;
			_this.password = '';
			_this.repassword = '';
		}
	}
};

angular.module('psApp').controller('ResetPassController', ResetPassController,[
	'$http',
	'$location',
	'Config']);