'use strict';

var HomeController = function(
	$location,
	$routeParams,
	$http,
	Config,
	Carousel
	) {
	var _this = this;
	_this.resetPassword = '';
	_this.error = '';
	_this.alerts = [];
	if ($routeParams.resetpass !== undefined) {
		_this.resetPassword = $routeParams.resetpass;
		var request = {
			hash: _this.resetPassword
		}
		$http({
      method: 'POST',
      url: Config.APPURL+'/api/validatehash',
      data: $.param(request),
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	  }).success(function(response) {
	   	_this.currentPartial = 'resetPass';
	  }).error(function(response) {
	  	_this.error = response.error;
	  	_this.currentPartial = 'main';
	  });
	} else {
		_this.currentPartial = 'main';
	}
	_this.carousel = Carousel;

	_this.ChangePartial = function(value){
		if ($location.$$search.resetpass !== undefined) {
			delete $location.$$search.resetpass;
			$location.path(value);
		}
		_this.currentPartial = value;
	};

	_this.ForgotPassword = function(form) {
		if(form.$valid){
			var request = {
	      email: _this.email
	    };

			$http({
	      method: 'POST',
	      url: Config.APPURL+'/api/forgotpass',
	      data: $.param(request),
	      headers: {
	        'Content-Type': 'application/x-www-form-urlencoded'
	      }
	    }).success(function(response) {
	    	_this.ChangePartial('main');
	      _this.alerts = [{type:'success', msg:'Se envió el email al usuario.'}];
	      GetUsers();
	    }).error(function(error) {
	      _this.alerts = [{type:'danger', msg:'Ocurrio un error y no se pudo completar el proceso.'}];
	    });
		}
	};

	_this.closeAlert = function(index) {
    _this.alerts.splice(index, 1);
  };
};

angular.module('psApp').controller('HomeController', HomeController, [
	'$location',
	'$routeParams',
	'$http',
	'Config',
	'Carousel'
	]);