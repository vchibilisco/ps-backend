'use strict';

var DashboardController = function($location, AuthService) {
	if(AuthService.IsStud()) $location.path('students-res');
	if(AuthService.IsDirect() && !AuthService.IsSecre() && !AuthService.IsAdmin()) $location.path('careers-res');
};

angular.module('psApp').controller('DashboardController', DashboardController, [
	'$location',
	'AuthService'
	]);