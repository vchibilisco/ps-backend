'use strict';

var SecretaryshipResConstroller = function(
  $uibModal, 
  $http, 
  $window, 
  $location, 
  HelpService,
  Config, 
  Views, 
  ResolutionResource) {
	this.resolutions =[];
  var that = this;

  var GetResolutions = function() {
    ResolutionResource.query({section:'secre-res'},function(response) {
      that.resolutions = response.resolutions;
    },function(error) {
      console.log(error);
    });
  };

  this.OpenModal = function(action, id){
		switch(action){
			case 'add':
				Open();
				break;
			case 'drop':
        Delete(id);
				break;
		}
	};

	var Open = function() {
		var modalInstance = $uibModal.open({
      animation: true,
      backdrop: 'static',
      templateUrl: Views.UPLOAD_RESOLUTION.modal_res,
      controller: Views.UPLOAD_RESOLUTION.controllermodal_res,
      resolve: {
        parent: function () {
          return {'section': 'secre-res'};
        }
      }
    });

    modalInstance.result.then(function (result) {
    	GetResolutions();
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
	};

 var Delete = function(id) {
    var modalInstance = $uibModal.open({
      animation: true,
      backdrop: 'static',
      templateUrl: Views.CONFIRMATION.deletemodal,
      controller: Views.CONFIRMATION.controller,
    });

    modalInstance.result.then(function (result) {
      if(result){
        ResolutionResource.delete({id:id},function() {
          GetResolutions();
        },function(error) {
          console.log(error);
        });
      }
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  this.OpenViewer = function(fileName) {
    HelpService.data = fileName;
    $location.path('viewer');
  };

  GetResolutions();
};

angular.module('psApp').controller('SecretaryshipResConstroller', SecretaryshipResConstroller,
  [
    '$uibModal',
    '$http',
    '$window',
    '$location',
    'HelpService',
    'Config',
    'Views',
    'ResolutionResource'
  ]);