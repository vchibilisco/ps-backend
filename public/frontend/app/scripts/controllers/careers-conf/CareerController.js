'use strict';

var CareerController = function(
  $uibModal, 
  CareerResource, 
  Views
) {
	this.careers = [];
  this.alerts = [];
	var that = this;
	var GetCareer = function() {
		var careerResource = new CareerResource();
		careerResource.$query().then(function(response) {
		  that.careers = response.careers;
		})
		.catch(function(error) {
		  console.log(error);
		});
	};
	
  this.OpenModal = function(action, id) {
  	switch(action){
  		case 'add':
  			Open(undefined);
  			break;
  		case 'edit':
  			var careerResource = new CareerResource();
				careerResource.$get({'id': id}).then(function(response) {
				  Open(response.career);
				})
				.catch(function(error) {
				  console.log(error);
				});
  			break;
  		case 'drop':
  			Delete(id);
  			break;
  	};
  };

  var Open = function(currentObject){
  	var modalInstance = $uibModal.open({
      animation: true,
      backdrop: 'static',
      templateUrl: Views.CAREER_MODULE.modal_conf,
      controller: Views.CAREER_MODULE.controllermodal_conf,
      resolve: {
        current: function () {
          return currentObject;
        }
      }
    });

    modalInstance.result.then(function (result) {
      if(result){
      	GetCareer();
      }
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  var Delete = function(id) {
  	var modalInstance = $uibModal.open({
      animation: true,
      backdrop: 'static',
      templateUrl: Views.CONFIRMATION.deletemodal,
      controller: Views.CONFIRMATION.controller,
    });

    modalInstance.result.then(function (result) {
      if(result){
      	CareerResource.delete({id:id}, function() {
          that.alerts = [{type:'success', msg:'Se eliminó registro correctamente.'}];
      		GetCareer();
      	}, function(error) {
      		that.alerts = [{type:'danger', msg:'Ocurrio un error y no se pudo completar el proceso.'}];
      	});
      }
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  this.closeAlert = function(index) {
    this.alerts.splice(index, 1);
  };

  GetCareer();
};

angular.module('psApp').controller('CareerController', CareerController,
  [
    '$uibModal',
    'CareerResource',
    'Views'
  ]);