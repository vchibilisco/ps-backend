'use strict';

var ModalCareerController = function($scope, $uibModalInstance, CareerResource, current) {
	$scope.hasError = false;
	var action = 'add';
	$scope.title = 'Nueva';
	if (current !== undefined) {
		action = 'edit';
		$scope.title = 'Modificar';
		$scope.description = current.description;	
	}
	
	$scope.Add = function (form) {
		if (form.$valid) {
			var request = {description: $scope.description};
			if (action === 'add') {
				CareerResource.save(request, function(response) {
	        $uibModalInstance.close(true);
	      }, function(error) {
	      	console.log(error);
	      });
			} else if(action === 'edit'){
			  CareerResource.update({id:current.id}, request, function(response) {
	        $uibModalInstance.close(true);
	      }, function(error) {
	      	console.log(error);
	      });
			}
		}else{
			$scope.hasError = true;
		}
	};
	$scope.Cancel = function () {
		$uibModalInstance.dismiss('cancel');
	}
};

angular.module('psApp').controller('ModalCareerController', ModalCareerController,['$scope','$uibModalInstance','CareerResource','current']);