'use strict';

var ReportsController = function(
	CareerResource,
	TypeResolutionResource,
	ReportsResource
	){
	var _this = this;
	_this.data = {};
	_this.careers = [];
	_this.typeresolutions = [];
	_this.enabledStudent = false;
	_this.results = [];
	_this.alerts = [];
	var init = function() {
		CareerResource.query(function(result) {
			_this.careers = result.careers;
			_this.careers.push({'id':'all','description':'Todos'});
		},function(error) {
			console.log(error);
		});
		var request = {
			area: 2
		};
		TypeResolutionResource.query(request, function(result) {
			_this.typeresolutions = result.resolutions;
		},function(error) {
			console.log(error);
		});
	};

	_this.Search = function() {
		if(_this.data.selectedcareer !== undefined && _this.data.selectedcareer !== ''){
			var request = {
				career: _this.data.selectedcareer,
				tresolution: (_this.data.selecttresolution !== undefined && _this.data.selecttresolution !== '') ? _this.data.selecttresolution : -1,
				status: (_this.enabledStudent) ? 1 : 0
			};
			ReportsResource.query(request, function(result){
				_this.results = result.students;
			},function(error) {
				_this.alerts = [{msg: error.error}];
			});
		}else{
			_this.alerts = [{msg:'Debe ingresar una Carrera'}];
		}
	};

	_this.closeAlert = function(index) {
    _this.alerts.splice(index, 1);
  };

  _this.GeneratePdf = function() {
		if(_this.results.length > 0){
			
			var career = _.find(_this.careers, function(element){ 
				if(element.id.toString() === _this.data.selectedcareer){
					return element.description;
				}
			});
			var tresolution = _.find(_this.typeresolutions, function(element){ 
				if(element.id.toString() === _this.data.selecttresolution){
					return element.description;
				}
			});

			var filter = 'Carrera: ' + career.description;
			if (tresolution !== undefined) {
				filter += ' T. Resolucion: ' + tresolution.description;
			}

			var columnsTable = [
				{ text: '#', style: 'tableHeader' },
				{ text: 'Apellido', style: 'tableHeader' },
				{ text: 'Nombre', style: 'tableHeader' },
				{ text: 'Expediente', style: 'tableHeader' }
			];
			var widths = [110, 110, 110, 110];
			if(_this.data.selectedcareer=='all') {
				columnsTable.push({text: 'Carrera', style: 'tableHeader'});
				widths = [100, 100, 100, 100, 100];
			}

			var docDefinition = { content: [
				{
					stack: [
						'Fecha: ' + moment().format('L')
					],
					style: 'datealig'
				},
				{
					image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACsAAAAfCAMAAABJaMRJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAACClBMVEUAAACzIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBezIBf///84SkVzAAAArHRSTlMAAAERLElgbnBtX0gqDhdRlMfl9Pr98+PBhjwJHXjQ+fCsPgMIXdL+63wNlfb7mRAesY8WsGIH6drb6tUiXPXimFo0MDVZqn0bzppFZqfM2dO5fjZUUu/8P9z4THFbqEfkz7IFTsQjJVhPzbtWJnYajnkKvt9oBrdyO/cSv5Ez5gJKJIFsjVDoeyB38kChMSlDpbQLuu1N7ICbZGmMhKLDXkav58KTNx8n7t09xqNPqgAAAAFiS0dErSBiwh0AAAAJcEhZcwAALiMAAC4jAXilP3YAAAIuSURBVDjLY2BABYxMzCysbOwcnFzcPLyMjAw4ASMjH7+AoJCwiKioqIiYuISklDQO5YyMMrJy8muQgYKikjI2xYwqqmrqa9CBhqYWIxbrtXXWYAG6evqM6EoNDNXXYAWiRiqoihmNTdbgAjqmKB5kNBNAsdfcwtJKF861tmFEdoGtHUKlvYOjkpOzi6ubGEzEHclgRg9PuEo7L28fXz//gMCg4BB/qJgcH5LaULiFYZz8guEQprhPRCSEFRWNpNYWFgZRMapWcCvEHNmgUSOJcARjLExtXHwCWEuCEEhVIo8ERDgpGVOtQkoSiEoNTUvPAHpBJFMTIp7FjKk2O0cYSKrlMgKBFNAxebEQcct8TLWSBSCyEOQ6xqLiNWtkXSESJTIYatVLA0GO5QZJMJaVr1F3rICa64GhNko1C0gKV4KMZeQpWSPkHAdR64np3qoMkFqRFEYGxuqQmjVrauvqIWoNMcNBJAfkhjW1DYyMjVVr1pQ3eUGDshlL+La0gkirNibGxnbPjs6uKohwdw9SvMHU9vKDZdub+/onOMVPhCpdM2kyFrXiSlMgATLVOrEEnk2mejNiUbtGe9p0jLQePgM5fyLUCvuwi6EpFZ3Ji5It4GrXWM2aLY6iVHiOPiMOtWsU5kbPQ7gjan6IGVrORFK7Jnz+goWyi9TsFwtZLlmawoxe8KCoBRq2JGbZ8s4V6fmTmTALHcaclWHIoFthlQ+uco/ReDUaiLDBohYA3Sq9ONnOL1oAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTYtMDQtMThUMDE6NDA6NTcrMDI6MDBfVA7HAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE2LTA0LTE4VDAxOjQwOjU3KzAyOjAwLgm2ewAAAABJRU5ErkJggg==',
					width: 50
				},
				{ text: 'Reporte SAR', style: 'subheader' },
				filter,
				'Total: ' + _this.results.length,
				{
						style: 'tableExample',
						table: {
								headerRows: 1,
								widths: widths,
								body: [
									columnsTable
								]
						}
				}
				]
					,
			styles: {
				datealig: {
					alignment: 'right'	
				},
				header: {
					fontSize: 18,
					bold: true,
					margin: [0, 0, 0, 10]
				},
				subheader: {
					fontSize: 16,
					bold: true,
					margin: [0, 10, 0, 5]
				},
				tableExample: {
					margin: [0, 5, 0, 15]
				},
				tableHeader: {
					bold: true,
					fontSize: 13,
					color: 'black'
				}
			},
			defaultStyle: {
			 }};

			 for (var i = 0; i < _this.results.length; i++) {
			 	var row = [
			 		_this.results[i].position.toString(),
			 		_this.results[i].lastname,
			 		_this.results[i].name,
			 		(_this.results[i].expedient != null || _this.results[i].expedient != undefined) ? _this.results[i].expedient : ''];
			 	if(_this.data.selectedcareer=='all') 
					row.push(_this.results[i].description);
			 	docDefinition.content[5].table.body.push(row);
			 }
			pdfMake.createPdf(docDefinition).open();
		}else{
			_this.alerts = [{msg:'No posee registros para generar el pdf.'}];
		}
  };

  _this.GenerateExcel = function() {
  	var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
    saveAs(blob, "Report.xls");
  };

	init();
};

angular.module('psApp').controller('ReportsController', ReportsController, [
	'CareerResource',
	'TypeResolutionResource',
	'ReportsResource'
	]);