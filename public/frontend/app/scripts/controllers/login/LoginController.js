'use strict';

var LoginController = function($http, $rootScope, $location,AuthService,Config) {
	this.dni = '';
	this.password = '';
	this.error = '';
	this.hasError = false;
	$rootScope.user = {
      IsLogin: false
  };
	this.Login = function(form, section) {
		if(form.$valid){
			var auth = {
				dni: this.dni,
				password: this.password,
				section: section
			};
			var that = this;
			$http({
	      method: 'POST',
	      url: Config.APPURL+'/api/authentication',
	      data: $.param(auth),
	      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		  }).success(function(response) {
		    $location.path('/dashboard');
		  }).error(function(response) {
		  	that.hasError = true;
	      that.error = response.error;
	      that.Clean();
		  });
		}else{
			this.hasError = true;
		}
	};
	this.Clean = function() {
		this.dni = '';
		this.password = '';
	};
};

angular.module('psApp').controller('LoginController', LoginController, 
	[
		'$http',
		'$rootScope',
		'$location',
		'AuthService',
		'Config']);