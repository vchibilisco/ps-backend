<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Bienvenido a Posgrado - Facultad de Ciencias Naturales e IML</h2>

<div>
    Estimado Usuario: le informamos su cuenta fue reiniciada.
		Ingrese al siguiente link para finalizar el proceso: <br>
    <a href="{!! $url !!}">Sistema de autogestión de Resoluciones</a> 
    <br><br>
    Saludos cordiales
</div>
<div style="text-align: center;">
	<img style="width: 40%;" src="{{ $message->embed(public_path() . '/frontend/app/images/LogoPosgradoCsNatRojo.png') }}"/>
</div>
</body>
</html>