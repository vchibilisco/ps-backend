<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Bienvenido a Posgrado - Facultad de Ciencias Naturales e IML</h2>

<div>
    Estimado/a: le informamos que se le creó una cuenta de usuario en el Sistema de autogestión de Resoluciones (SAR) de la Secretaría de Posgrado de la Facultad de Ciencias Naturales e IML.
		Este sistema informático web le permitirá acceder a las resoluciones de forma digital. A medida que el sistema se actualice, el mismo enviará notificaciones a su dirección de mail sobre las mencionadas actualizaciones, de manera tal que Ud. con su nombre de Usuario (DNI) y contraseña pueda ingresar y consultarlas.
		Ingrese al siguiente link para finalizar con la registración: <br>
    <a href="{!! $url !!}">Sistema de autogestión de Resoluciones</a> 
    <br><br>
    Saludos cordiales
</div>
<div style="text-align: center;">
	<img style="width: 40%;" src="{{ $message->embed(public_path() . '/frontend/app/images/LogoPosgradoCsNatRojo.png') }}"/>
</div>
</body>
</html>