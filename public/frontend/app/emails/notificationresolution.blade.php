<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Bienvenido a Posgrado - Facultad de Ciencias Naturales e IML</h2>

<div>
		Estimado alumno/a: 
		<br>
		Se realizó una actualización de las resoluciones vinculadas con su legajo en SAR de la Secretaría de Posgrado de la Facultad de Ciencias Naturales e IML.
		Ingrese con su DNI y contraseña, y consulte las resoluciones cargadas
 		<br>
    <a href="{!! $url !!}">Sistema de autogestión de Resoluciones</a> 
    <br><br>
    Saludos Cordiales
</div>
<div style="text-align: center;">
	<img style="width: 40%;" src="{{ $message->embed(public_path() . '/frontend/app/images/LogoPosgradoCsNatRojo.png') }}"/>
</div>

</body>
</html>